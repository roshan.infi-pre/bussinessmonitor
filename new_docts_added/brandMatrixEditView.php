<?

require_once 'XTemplate/xtpl.php';
//    require_once('modules/TourPlans/BrandPlan.php');
//    require_once('themes/'.$theme.'/layout_utils.php');
require_once 'include/utils.php';
require_once 'include/uifromdbutil.php';
require_once 'include/database/PearDatabase.php';
global $app_strings;
global $app_list_strings;
global $mod_strings;
global $list_max_entries_per_page;
global $urlPrefix;
global $currentModule;
global $image_path;
global $theme;
global $focus_list;
global $adb;
global $current_user;
global $theme;
//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";
if ($_REQUEST['mode'] == 'edit') {
    $search_form = new XTemplate('modules/TourPlans/brandMatrixHeader.html');
    $qry         = "select users.first_name,users.last_name,patches.patchname,patches.groupid as gid from users inner join patches on users.patch=patches.patchid where users.id='" . $current_user->id . "'";
    $qryres      = $adb->query($qry);
    $name        = $adb->query_result($qryres, 0, 'first_name') . ' ' . $adb->query_result($qryres, 0, 'last_name');
    $group_id    = $adb->query_result($qryres, 0, 'gid');

    if ($group_id != 0) {
        $innerjoin   = "inner join group2brand on productbrandtype.productbrandtypeid=group2brand.brandid ";
        $whereclause = "and group2brand.deleted=0 and group2brand.grpid='" . $group_id . "'";
    }
    ?>
	<script type="text/javascript">
		brandarray=new Array();
	</script>
	<?php
$brandq = "select productbrandtypeid, productbrandtype from productbrandtype
		INNER JOIN brand_vs_contents ON productbrandtype.productbrandtypeid = brand_vs_contents.brand_id
		INNER JOIN content ON content.id = brand_vs_contents.content_id
		" . $innerjoin . "
		where presence=1 and  division='" . $current_user->division . "'  and brand_vs_contents.del_flag=0 and content.del_flag=0 " . $whereclause . " group by productbrandtype.productbrandtypeid";
    $brandr = $adb->query($brandq);
    for ($b = 0; $b < $adb->num_rows($brandr); $b++) {
        $brandid = $adb->query_result($brandr, $b, 'productbrandtypeid');
        ?>
		<script type="text/javascript">
			<?echo "brandarray.push(\"$brandid\")"; ?>
		</script>
		<?php
}

    $brandmatrixq = "select * from brandmatrixplanningmain where mainid='" . $_REQUEST['record'] . "'";
    $brandmatrixr = $adb->query($brandmatrixq);
    //dg
    // if ($adb->query_result($brandmatrixr,0,'doctorlistname') == 1) {
    // $search_form->assign("DOCLISTUPONEDIT", 'Entire list');
    // } else if($adb->query_result($brandmatrixr,0,'doctorlistname') == 2) {
    // $search_form->assign("DOCLISTUPONEDIT", 'My fourthy list');
    // } else if($adb->query_result($brandmatrixr,0,'doctorlistname') == 3) {
    // $search_form->assign("DOCLISTUPONEDIT", 'L 40 list');
    // } //dg
    //dg
    if (isset($_REQUEST['next']) || isset($_REQUEST['previous'])) {
        $doctorlist = $_REQUEST['doctorlistname'];
        if ($_REQUEST['doctorlistname'] == 1) {
            $a = 'selected="selected"';
            $b = '';
            $c = '';
        } else if ($_REQUEST['doctorlistname'] == 2) {
            $a = '';
            $b = 'selected="selected"';
            $c = '';
        } else if ($_REQUEST['doctorlistname'] == 3) {
            $a = '';
            $b = '';
            $c = 'selected="selected"';
        }
    } else {
        if ($_REQUEST['doclist'] == 1) {
            $a = 'selected="selected"';
            $b = '';
            $c = '';
        } else if ($_REQUEST['doclist'] == 2) {
            $a = '';
            $b = 'selected="selected"';
            $c = '';
        } else if ($_REQUEST['doclist'] == 3) {
            $a = '';
            $b = '';
            $c = 'selected="selected"';
        }
    }

    $drlistoption = '<option value="1" ' . $a . '>Entire list</option>
		<option value="2" ' . $b . '>My Core list</option>

		';
    $search_form->assign("DOCTORLISTOPTION", $drlistoption);
    //dg

    $search_form->assign("BDENAME", $name);
    $search_form->assign("TERRNAME", $adb->query_result($qryres, 0, 'patchname'));
    // dg $search_form->assign("DISPLAYDOCLIST", 'display: none;');
    $search_form->assign("FYEAR", $adb->query_result($brandmatrixr, 0, 'year'));
    $search_form->assign("CYCLE", $adb->query_result($brandmatrixr, 0, 'cycle'));
    $search_form->assign("MODE", $_REQUEST['mode']); //dg
    $search_form->assign("RECORD", $_REQUEST['record']); //dg
    $search_form->parse("main");
    $search_form->out("main");
    if ($adb->query_result($brandmatrixr, 0, 'doctorlistname')) {
        $list_form = new XTemplate('modules/TourPlans/brandMatrixList.html');
        // $sql="select * from brandmatrixplanningmain inner join brandmatrixplanning on brandmatrixplanningmain.mainid=brandmatrixplanning.mainid where brandmatrixplanningmain.mainid='".$_REQUEST['record']."'  and brandmatrixplanningmain.deleted=0 and brandmatrixplanning.deleted=0";
        if ($_REQUEST['doclist'] == 2 || $doctorlist == 2) {
            //$mslqry = "select crmid from msllist where smownerid='".$current_user->id."' and fyear='".$_REQUEST['fyear']."' and deleted=0";
            $mslqry    = "select max(crmid) as crmid from msllist where deleted=0 and smownerid = '" . $current_user->id . "' and msllist.authorise=2 and msllist.submitted=1 group by smownerid order by smownerid";
            $resmslqry = $adb->query($mslqry);
            $crmid     = $adb->query_result($resmslqry, 0, 'crmid');
            $sql       = "select contactdetails.*,brandmatrixplanning.brandsfocussed, speciality.speciality,  visitfrequency.frequencyname
						from msllist
						left outer join msldetails on msldetails.id = msllist.crmid
						left outer join bricks on msldetails.patchid = bricks.brickid
						inner join contactdetails on contactdetails.contactid = msldetails.contactid
						inner join users on msldetails.contactownerid=users.id
						inner join patches on users.patch=patches.patchid
						left outer join class on classid=contactdetails.class
						left outer join speciality on specialityid=contactdetails.speciality
						left outer join visitfrequency on frequencyid=contactdetails.frequency
						left outer join city on city.cityid=contactdetails.mailingcity
						left outer join qualification on contactdetails.qualification=qualification.qualificationid
						left outer join brandmatrixplanning on contactdetails.contactid = brandmatrixplanning.contactid
						left outer join brandmatrixplanningmain on brandmatrixplanningmain.mainid=brandmatrixplanning.mainid
						where (dr_type!='D' or dr_type is null) and msllist.smownerid IN (" . $current_user->id . ") and msllist.crmid='" . $crmid . "'  and msllist.deleted=0 and msldetails.del=0 and brandmatrixplanning.deleted=0 and brandmatrixplanningmain.mainid='" . $_REQUEST['record'] . "'  and brandmatrixplanningmain.deleted=0 and contactdetails.deleted=0 and msllist.authorise=2 and msllist.submitted=1 ORDER BY contactdetails.firstname ASC";

        } else if ($_REQUEST['doclist'] == 1 || $doctorlist == 1) {
            $sql = "select contactdetails.*,brandmatrixplanning.brandsfocussed, speciality.speciality,  visitfrequency.frequencyname from contactdetails left outer join users on contactdetails.smownerid=users.id left outer join user2role on users.id=user2role.userid left outer join qualification on contactdetails.qualification=qualification.qualificationid left outer join speciality on contactdetails.speciality=speciality.specialityid left outer join class on contactdetails.class=class.classid left outer join visitfrequency on contactdetails.frequency=visitfrequency.frequencyid left outer join city on city.cityid=contactdetails.mailingcity left outer join bricks on contactdetails.brick=bricks.brickid
					left outer join brandmatrixplanning on (contactdetails.contactid = brandmatrixplanning.contactid and brandmatrixplanning.mainid='" . $_REQUEST['record'] . "')
					left outer join brandmatrixplanningmain on (brandmatrixplanningmain.mainid=brandmatrixplanning.mainid and brandmatrixplanningmain.deleted=0)
					where contactdetails.deleted=0 AND  contactdetails.smownerid='" . $current_user->id . "'  ORDER BY contactdetails.firstname ASC";
        } else if ($_REQUEST['doclist'] == 3 || $doctorlist == 3) {
            $sql = "select contactdetails.*, brandmatrixplanning.brandsfocussed, speciality.speciality,  visitfrequency.frequencyname
				from campaignmain
				inner join campaigndetails
				on campaignmain.crmid=campaigndetails.id
				inner join users
				on users.id=campaignmain.smcreatorid
				inner join patches
				on users.patch=patches.patchid
				inner join division
				on users.division=division.divisionid
				inner join campaigns
				on campaigns.pid=campaignmain.campaignid
				inner join contactdetails on contactdetails.contactid=campaigndetails.contactid
				left outer join qualification on contactdetails.qualification=qualification.qualificationid left outer join speciality on contactdetails.speciality=speciality.specialityid left outer join class on contactdetails.class=class.classid
				left outer join visitfrequency on contactdetails.frequency=visitfrequency.frequencyid left outer join city on city.cityid=contactdetails.mailingcity left outer join bricks on contactdetails.brick=bricks.brickid
				left outer join brandmatrixplanning on contactdetails.contactid = brandmatrixplanning.contactid
				left outer join brandmatrixplanningmain on brandmatrixplanningmain.mainid=brandmatrixplanning.mainid
				where campaignmain.deleted=0 and campaigndetails.del=0
				and contactdetails.smownerid='" . $current_user->id . "' and  brandmatrixplanningmain.mainid='" . $_REQUEST['record'] . "'  and brandmatrixplanningmain.deleted=0  and brandmatrixplanning.deleted=0 and contactdetails.deleted=0
				group by smcreatorid,campaignid,contactdetails.contactid
				order by contactdetails.firstname";

        } else {
            $sql = "select * from brandmatrixplanningmain inner join brandmatrixplanning on brandmatrixplanningmain.mainid=brandmatrixplanning.mainid inner join contactdetails on contactdetails.contactid = brandmatrixplanning.contactid left outer join speciality on specialityid=contactdetails.speciality left outer join visitfrequency on frequencyid=contactdetails.frequency where brandmatrixplanningmain.mainid='" . $_REQUEST['record'] . "'  and brandmatrixplanningmain.deleted=0 and brandmatrixplanning.deleted=0 and contactdetails.deleted=0 order by contactdetails.firstname ";
        }

        $sqlr           = $adb->query($sql);
        $focbrandcountq = "select noofbrands from brandmatrixmaster where division='" . $current_user->division . "' and year='" . $adb->query_result($brandmatrixr, 0, 'year') . "'";
        $focbrandcountr = $adb->query($focbrandcountq);
        $noofbrands     = $adb->query_result($focbrandcountr, 0, 'noofbrands');
        $focussedbrand  = '';
        for ($i = 0; $i < $noofbrands; $i++) {
            $a = $i + 1;
            $focussedbrand .= '<th>Focussed Brand ' . $a . '</th>';
        }
        $list_form->assign("FOCUSSEDBRAND", $focussedbrand);
        //to take all hidden fields including brands
        $hfields = '<div style="display: ;"><table id="detaildata">';
        for ($h = 0; $h < $adb->num_rows($sqlr); $h++) {
            $x = $h + 1;
            $hfields .= '<tr>';
            //echo "<br>x:".$x."c:".$adb->query_result($sqlr,$h,'contactid');
            $hfields .= '<td><input type="hidden" id="hcontact_' . $x . '" name="hcontact_' . $x . '" value="' . $adb->query_result($sqlr, $h, 'contactid') . '" /></td><td>';
            $brands = explode(",", $adb->query_result($sqlr, $h, 'brandsfocussed'));
            for ($g = 0; $g < $noofbrands; $g++) {
                if (count($brands) > $g) {
                    if ($brands[$g]) {
                        $val = $brands[$g];
                    } else {
                        $val = '';
                    }

                } else {
                    $val = '';
                }

                $hfields .= '<input type="hidden" name="hbrand_' . $x . '_' . $g . '" id="hbrandid_' . $x . '_' . $g . '" value="' . $val . '" />';
            }
            $hfields .= '</td></tr>';
        }
        $hfields .= '</table></div>';
        /*
        if ($_REQUEST['next'] || $_REQUEST['previous']) {
        $hfields = '<div style="display:;"><table>';
        for ($h=0; $h<$_REQUEST['allcount']; $h++) {
        $x = $h+1;
        $hfields .='<tr>';
        $hfields .='<td><input type="text" name="hcontact_'.$x.'" value="'.$_REQUEST['hcontact_'.$x].'" /></td><td>';
        for ($g=0; $g<$noofbrands; $g++) {
        $hfields .= '<input type="hidden" name="hbrand_'.$x.'_'.$g.'" id="hbrandid_'.$x.'_'.$g.'" value="'.$_REQUEST['hbrand_'.$x.'_'.$g].'" />';
        }
        $hfields .='</td></tr>';
        }
        $hfields .= '</table></div>';
        $list_form->assign("HDIV", $hfields);
        $list_form->assign("HDIVCOUNT",$_REQUEST['allcount']);
        } else { *///dg
        $list_form->assign("HDIV", $hfields);
        $list_form->assign("HDIVCOUNT", $adb->num_rows($sqlr));
        //} //dg

        $list_form->assign("CONSTANTBRANDSCOUNT", $noofbrands);
        $list_form->assign("FYEAR", $adb->query_result($brandmatrixr, 0, 'year'));
        //    $list_form->assign("DRLISTNAME",$adb->query_result($brandmatrixr,0,'doctorlistname'));
        $list_form->assign("RECORD", $_REQUEST['record']);
        $list_form->assign("EDITEDMODE", 1);
        $list_form->assign("MODE", 'edit');

        //dg if ($adb->query_result($brandmatrixr,0,'doctorlistname')) {
        // $list_form->assign("DOCTORLISTNAME",$adb->query_result($brandmatrixr,0,'doctorlistname'));
        // } else {
        // $list_form->assign("DOCTORLISTNAME",$_REQUEST['doctorlistname']);
        //dg }
        if ($_REQUEST['doclist']) {
            $list_form->assign("DOCTORLISTNAME", $_REQUEST['doclist']);
            $iListName = $_REQUEST['doclist'];
        } else {
            $list_form->assign("DOCTORLISTNAME", $_REQUEST['doctorlistname']);
            $iListName = $_REQUEST['doctorlistname'];
        }
        //echo "iListName".$iListName;
        /*
        if ($_REQUEST['doclist'] == 1 || $doctorlist == 1) {
        $sql="select * from brandmatrixplanningmain inner join brandmatrixplanning on brandmatrixplanningmain.mainid=brandmatrixplanning.mainid inner join contactdetails on contactdetails.contactid = brandmatrixplanning.contactid left outer join speciality on specialityid=contactdetails.speciality left outer join visitfrequency on frequencyid=contactdetails.frequency where brandmatrixplanningmain.mainid='".$_REQUEST['record']."'  and brandmatrixplanningmain.deleted=0 and brandmatrixplanning.deleted=0 ";
        } elseif ($_REQUEST['doclist'] == 2 || $doctorlist == 2) {
        $mslqry = "select crmid from msllist where smownerid='".$current_user->id."' and fyear='".$_REQUEST['fyear']."' and deleted=0";
        $resmslqry = $adb->query($mslqry);
        $crmid = $adb->query_result($resmslqry,0,'crmid');
        $sql="select * from brandmatrixplanningmain inner join brandmatrixplanning on brandmatrixplanningmain.mainid=brandmatrixplanning.mainid inner join contactdetails on contactdetails.contactid = brandmatrixplanning.contactid left outer join speciality on specialityid=contactdetails.speciality left outer join visitfrequency on frequencyid=contactdetails.frequency
        inner join msldetails on contactdetails.contactid = msldetails.contactid
        inner join msllist on msldetails.id = msllist.crmid
        where msllist.crmid='".$crmid."' and msllist.deleted=0 and msldetails.del=0 and brandmatrixplanningmain.mainid='".$_REQUEST['record']."'  and brandmatrixplanningmain.deleted=0 and brandmatrixplanning.deleted=0 ";
        } elseif ($_REQUEST['doclist'] == 3 || $doctorlist == 3) {
        $sql="select * from brandmatrixplanningmain inner join brandmatrixplanning on brandmatrixplanningmain.mainid=brandmatrixplanning.mainid inner join contactdetails on contactdetails.contactid = brandmatrixplanning.contactid left outer join speciality on specialityid=contactdetails.speciality left outer join visitfrequency on frequencyid=contactdetails.frequency inner join campaigndetails on contactdetails.contactid=campaigndetails.contactid
        inner join campaignmain on campaignmain.crmid=campaigndetails.id
        where contactdetails.smownerid='".$current_user->id."' and campaignmain.deleted=0 and campaigndetails.del=0 and brandmatrixplanningmain.mainid='".$_REQUEST['record']."'  and brandmatrixplanningmain.deleted=0 and brandmatrixplanning.deleted=0 ";
        } else {
        $sql="select * from brandmatrixplanningmain inner join brandmatrixplanning on brandmatrixplanningmain.mainid=brandmatrixplanning.mainid inner join contactdetails on contactdetails.contactid = brandmatrixplanning.contactid left outer join speciality on specialityid=contactdetails.speciality left outer join visitfrequency on frequencyid=contactdetails.frequency where brandmatrixplanningmain.mainid='".$_REQUEST['record']."'  and brandmatrixplanningmain.deleted=0 and brandmatrixplanning.deleted=0 ";
        } */

        //echo $sql;

        $sqlrecnew    = $adb->query($sql);
        $recordscount = $adb->num_rows($sqlrecnew);

        //to get count of records
        //$recordscount = $adb->num_rows($sqlr);
        //per page records
        $pagerecords = 20;
        $last        = $recordscount / $pagerecords;
        if (!$_REQUEST['pagenum']) {
            $pagenum = 0;
        } else {
            if ($_REQUEST['next']) {
                $pagenum = $_REQUEST['pagenum'];
            } else if ($_REQUEST['previous']) {
                $pagenum = $_REQUEST['pageprevious'];
            }

        }

        if (!$_REQUEST['pagenum']) {
            $list_form->assign("PAGENUM", 2);
            $list_form->assign("PAGENUMPREV", 0);
        } else if ($_REQUEST['pagenum'] && $_REQUEST['next']) {
            $no  = $_REQUEST['pagenum'] + 1;
            $no1 = $_REQUEST['pageprevious'] + 1;
            $list_form->assign("PAGENUM", $no);
            $list_form->assign("PAGENUMPREV", $no1);
        } else if ($_REQUEST['pageprevious'] && $_REQUEST['previous']) {
            $no1 = $_REQUEST['pagenum'] - 1;
            $no  = $_REQUEST['pageprevious'] - 1;
            $list_form->assign("PAGENUM", $no1);
            $list_form->assign("PAGENUMPREV", $no);
        }

        if ($pagenum < 1) {
            $pagenum = 1;
        } else if ($pagenum > $last) {
            $pagenum = $last;
        }

        $list_form->assign("DISABLEPREVIOUS", '');
        $list_form->assign("DISABLENEXT", '');
        if ($pagenum == 1) {
            $list_form->assign("DISABLEPREVIOUS", 'disabled="disabled"');
        } else if ($pagenum == $last) {
            $list_form->assign("DISABLENEXT", 'disabled="disabled"');
        }
        $no = ceil($pagenum);
        if ($no == 0) {
            $no = 1;
        }
        $list_form->assign("PAGENUMBER", 'Page ' . $no . ' of ' . ceil($last));
        $max = 'limit ' . ($pagenum - 1) * $pagerecords . ',' . $pagerecords;
        //dg
        // $sql="select * from brandmatrixplanningmain inner join brandmatrixplanning on brandmatrixplanningmain.mainid=brandmatrixplanning.mainid inner join contactdetails on contactdetails.contactid = brandmatrixplanning.contactid left outer join speciality on specialityid=contactdetails.speciality left outer join visitfrequency on frequencyid=contactdetails.frequency where brandmatrixplanningmain.mainid='".$_REQUEST['record']."'  and brandmatrixplanningmain.deleted=0 and brandmatrixplanning.deleted=0 ".$max;
        // dg

        $sql = $sql . " " . $max;
        //echo $sql."<br>";

        $sqlrec = $adb->query($sql);

        //to get all records
        $list = '';
        $srno = ($pagenum - 1) * $pagerecords;
        $srno = $srno + 1;
        //$mquery = "select crmid from msllist where smownerid='".$current_user->id."' and fyear='".$adb->query_result($brandmatrixr,0,'financialyear')."' and deleted=0";
        $mquery = "select max(crmid) as crmid from msllist where deleted=0 and msllist.authorise=2 and msllist.submitted=1 and smownerid = '" . $current_user->id . "' group by smownerid order by smownerid";

        $mqueryrs = $adb->query($mquery);
        for ($i = 0; $i < $adb->num_rows($sqlrec); $i++) {

            $msquery = "select contactdetails.contactid from  msllist left outer join msldetails on msldetails.id = msllist.crmid  inner join contactdetails on contactdetails.contactid = msldetails.contactid  where (dr_type!='D' or dr_type is null) and msllist.crmid='" . $adb->query_result($mqueryrs, 0, 'crmid') . "'  and msllist.smownerid IN (" . $current_user->id . ") and msllist.deleted=0 and msldetails.del=0 and msllist.authorise=2 and msllist.submitted=1 and contactdetails.contactid='" . $adb->query_result($sqlrec, $i, 'contactid') . "'";
            //echo $msquery."<br>";
            $msqueryrs = $adb->query($msquery);
            $listname  = '';
            if ($adb->num_rows($msqueryrs) > 0) {
                $listname = 'M 40';
            } else {
                /* dg
                if ($adb->query_result($brandmatrixr,0,'doctorlistname') == 1) {
                $listname = 'Entire List';
                } else if ($adb->query_result($brandmatrixr,0,'doctorlistname') == 2) {
                $listname = 'M 40';
                } else if ($adb->query_result($brandmatrixr,0,'doctorlistname') == 3) {
                $listname = 'L 40';
                } dg    */
                if ($iListName == 1 || trim($iListName) == '') {
                    $listname = 'Entire List';
                } else if ($iListName == 2) {
                    $listname = 'M 40';
                } else if ($iListName == 3) {
                    $listname = 'L 40';
                }
            }

            $list .= '<tr>';
            $list .= '<td>' . $srno . '</td>';
            $list .= '<td>Dr. ' . $adb->query_result($sqlrec, $i, 'firstname') . ' ' . $adb->query_result($sqlrec, $i, 'middlename') . ' ' . $adb->query_result($sqlrec, $i, 'lastname') . '</td>';
            $list .= '<td>' . $adb->query_result($sqlrec, $i, 'speciality') . '</td>';
            $list .= '<td>' . $adb->query_result($sqlrec, $i, 'frequencyname') . '</td>';
            $list .= '<td>' . $listname . '</td>';
            //dg
            // if ($_REQUEST['next'] || $_REQUEST['previous']) {
            // for ($h=0; $h<$_REQUEST['allcount']; $h++) {
            // $x = $h+1;
            // if ($x == $srno) {
            // for ($g=0; $g<$noofbrands; $g++) {
            // $brandq = "select productbrandtypeid, productbrandtype from productbrandtype where division='".$current_user->division."'";
            // $brandr = $adb->query($brandq);
            // $brandoption = '';
            // for ($b=0; $b<$adb->num_rows($brandr); $b++) {
            // if ($_REQUEST['hbrand_'.$x.'_'.$g]==$adb->query_result($brandr,$b,'productbrandtypeid')) {
            // $select = 'selected="selected"';
            // } else {
            // $select = '';
            // }
            // $brandoption .= '<option value="'.$adb->query_result($brandr,$b,'productbrandtypeid').'" '.$select.'>'.$adb->query_result($brandr,$b,'productbrandtype').'</option>';
            // }
            // $list .= '<td>
            // <select name="brand_'.$srno.'" id="brand_'.$srno.'_'.$g.'" onClick="updateField(this.id)" onBlur="updateField(this.id)">
            // <option style="background:yellow" value="">Select Brand</option>
            // '.$brandoption.'
            // </select>
            // </td>';
            // }
            // $list .= '</tr>';
            // }
            // }
            // } else { //dg
            $brandsselected = explode(",", $adb->query_result($sqlrec, $i, 'brandsfocussed'));
            for ($j = 0; $j < $noofbrands; $j++) {
                //to build brand dropdown
                $brandq = "select productbrandtypeid, productbrandtype from productbrandtype
					INNER JOIN brand_vs_contents ON productbrandtype.productbrandtypeid = brand_vs_contents.brand_id
					INNER JOIN content ON content.id = brand_vs_contents.content_id
					" . $innerjoin . "
					where presence=1 and  division='" . $current_user->division . "'  and brand_vs_contents.del_flag=0 and content.del_flag=0 " . $whereclause . " group by productbrandtype.productbrandtypeid";
                $brandr      = $adb->query($brandq);
                $brandoption = '';
                for ($b = 0; $b < $adb->num_rows($brandr); $b++) {
                    if (count($brandsselected) > $j) {
                        if ($brandsselected[$j] == $adb->query_result($brandr, $b, 'productbrandtypeid')) {
                            $select = 'selected="selected"';
                        } else {
                            $select = '';
                        }
                    } else {
                        $select = '';
                    }

                    $brandoption .= '<option value="' . $adb->query_result($brandr, $b, 'productbrandtypeid') . '" ' . $select . '>' . $adb->query_result($brandr, $b, 'productbrandtype') . '</option>';
                }
                $list .= '<td>
					<select name="brand_' . $srno . '" id="brand_' . $srno . '_' . $j . '" onClick="updateField(this.id)" onBlur="updateField(this.id)">
					<option style="background:yellow" value="">Select Brand</option>
					' . $brandoption . '
					</select>
					</td>';
            }
            $list .= '</tr>';
            //} //dg
            $srno++;
        }
        $list_form->assign("LISTOFRECORDS", $list);
        $list_form->parse("main");
        $list_form->out("main");
    }
} else {
    //new

    $maxid_query = "select max(mainid) as maxid from brandmatrixplanningmain where deleted=0 and smownerid = '" . $current_user->id . "'
				and status=2";
    $sqlres = $adb->query($maxid_query);
    $maxid  = $adb->query_result($sqlres, 0, 'maxid');

    $qry      = "select users.first_name,users.last_name,patches.patchname,patches.groupid as gid from users inner join patches on users.patch=patches.patchid where users.id='" . $current_user->id . "'";
    $qryres   = $adb->query($qry);
    $name     = $adb->query_result($qryres, 0, 'first_name') . ' ' . $adb->query_result($qryres, 0, 'last_name');
    $group_id = $adb->query_result($qryres, 0, 'gid');

    if ($group_id != 0) {
        $innerjoin   = "inner join group2brand on productbrandtype.productbrandtypeid=group2brand.brandid ";
        $whereclause = "and group2brand.deleted=0 and group2brand.grpid='" . $group_id . "'";
    }

    ?>
	<script type="text/javascript">
		brandarray=new Array();
	</script>
	<?php
$brandq = "select productbrandtypeid, productbrandtype from productbrandtype
		INNER JOIN brand_vs_contents ON productbrandtype.productbrandtypeid = brand_vs_contents.brand_id
		INNER JOIN content ON content.id = brand_vs_contents.content_id
		" . $innerjoin . "
		where presence=1 and  division='" . $current_user->division . "'  and brand_vs_contents.del_flag=0 and content.del_flag=0 " . $whereclause . " group by productbrandtype.productbrandtypeid";
    $brandr = $adb->query($brandq);
    for ($b = 0; $b < $adb->num_rows($brandr); $b++) {
        $brandid = $adb->query_result($brandr, $b, 'productbrandtypeid');
        ?>

		<script type="text/javascript">
			<?echo "brandarray.push(\"$brandid\")"; ?>
		</script>
		<?php
}

    $search_form = new XTemplate('modules/TourPlans/brandMatrixHeader.html');
    $qry         = "select users.first_name,users.last_name,patches.patchname from users inner join patches on users.patch=patches.patchid where users.id='" . $current_user->id . "'";
    $qryres      = $adb->query($qry);
    $name        = $adb->query_result($qryres, 0, 'first_name') . ' ' . $adb->query_result($qryres, 0, 'last_name');
    $doctorlist  = '';
    if (isset($_REQUEST['next']) || isset($_REQUEST['previous'])) {
        $doctorlist = $_REQUEST['doctorlistname'];
        if ($_REQUEST['doctorlistname'] == 1) {
            $a = 'selected="selected"';
            $b = '';
            $c = '';
        } else if ($_REQUEST['doctorlistname'] == 2) {
            $a = '';
            $b = 'selected="selected"';
            $c = '';
        } else if ($_REQUEST['doctorlistname'] == 3) {
            $a = '';
            $b = '';
            $c = 'selected="selected"';
        }
    } else {
        if ($_REQUEST['doclist'] == 1) {
            $a = 'selected="selected"';
            $b = '';
            $c = '';
        } else if ($_REQUEST['doclist'] == 2) {
            $a = '';
            $b = 'selected="selected"';
            $c = '';
        } else if ($_REQUEST['doclist'] == 3) {
            $a = '';
            $b = '';
            $c = 'selected="selected"';
        }
    }
    $drlistoption = '<option value="1" ' . $a . '>Entire list</option>
		<option value="2" ' . $b . '>My Core list</option>

		';
    $search_form->assign("BDENAME", $name);
    $search_form->assign("TERRNAME", $adb->query_result($qryres, 0, 'patchname'));
    $search_form->assign("DOCTORLISTOPTION", $drlistoption);
    $search_form->assign("DOCLISTUPONEDIT", '');
    $search_form->assign("DISPLAYDOCLIST", '');

    if ($_REQUEST['fyear1']) {
        $search_form->assign("FYEAR", $_REQUEST['fyear1']);
    } else {
        $search_form->assign("FYEAR", $_REQUEST['fyear']);
    }

    $search_form->assign("CYCLE", $_REQUEST['cycle']);
    $search_form->assign("MODE", 'new'); //dg
    $search_form->assign("RECORD", $_REQUEST['record']); //dg
    $search_form->parse("main");
    $search_form->out("main");
    if ($_REQUEST['doclist'] || $doctorlist != '') {
        $list_form = new XTemplate('modules/TourPlans/brandMatrixList.html');
        if ($_REQUEST['doclist'] == 2 || $doctorlist == 2) {
            //$mslqry = "select crmid from msllist where smownerid='".$current_user->id."' and fyear='".$_REQUEST['fyear']."' and deleted=0";
            $mslqry    = "select max(crmid) as crmid from msllist where deleted=0 and smownerid = '" . $current_user->id . "' and msllist.authorise=2 and msllist.submitted=1 group by smownerid order by smownerid";
            $resmslqry = $adb->query($mslqry);
            $crmid     = $adb->query_result($resmslqry, 0, 'crmid');
            $sql       = "select
				contactdetails.deleted as delcontact,
				bricks.brickname,
				msldetails.patchid,
				msldetails.contactownerid as userid,
				msldetails.pid,
				contactdetails.contactid as id,
				contactdetails.firstname,
				contactdetails.middlename,
				contactdetails.lastname,
				qualificationname,
				speciality.speciality,
				specialityname,
				classname,
				visitfrequency.frequencyname,
				city.cityname,
				accountname,
				case when users.rx_type='Regular' then concat_ws(' ',users.first_name,users.middle_name,users.last_name) else patches.patchname end as username ,
				msldetails.dr_type as type,
				msldetails.delrsn as delrsn
				from msllist
				left outer join msldetails on msldetails.id = msllist.crmid
				left outer join bricks on msldetails.patchid = bricks.brickid
				inner join contactdetails on contactdetails.contactid = msldetails.contactid
				inner join users on msldetails.contactownerid=users.id
				inner join patches on users.patch=patches.patchid
				left outer join class on classid=contactdetails.class
				left outer join speciality on specialityid=contactdetails.speciality
				left outer join visitfrequency on frequencyid=contactdetails.frequency
				left outer join city on city.cityid=contactdetails.mailingcity
				left outer join qualification on contactdetails.qualification=qualification.qualificationid
				where msllist.crmid='" . $crmid . "'  and msllist.authorise=2 and msllist.submitted=1 and msllist.deleted=0 and msldetails.del=0 ORDER BY contactdetails.firstname ASC";
            $sqlr = $adb->query($sql);
        } else if ($_REQUEST['doclist'] == 1 || $doctorlist == 1) {
            $sql = "select contactdetails.contactid as id,contactdetails.doc_code,contactdetails.contactid as crmid,contactdetails.firstname,contactdetails.middlename,contactdetails.lastname,qualificationname,speciality.speciality,specialityname,classname,brickname,users.rx_type,smownerid,concat_ws(' ',users.first_name,last_name) as user_name,visitfrequency.frequencyname,city.cityname,accountname,contactdetails.edit_available,masterid,updated from contactdetails left outer join users on contactdetails.smownerid=users.id left outer join user2role on users.id=user2role.userid left outer join qualification on contactdetails.qualification=qualification.qualificationid left outer join speciality on contactdetails.speciality=speciality.specialityid left outer join class on contactdetails.class=class.classid left outer join visitfrequency on contactdetails.frequency=visitfrequency.frequencyid left outer join city on city.cityid=contactdetails.mailingcity left outer join bricks on contactdetails.brick=bricks.brickid where contactdetails.deleted=0 AND  contactdetails.smownerid='" . $current_user->id . "'  ORDER BY contactdetails.firstname ASC";

            // $sql="select contactdetails.*,brandmatrixplanning.brandsfocussed, speciality.speciality,  visitfrequency.frequencyname from contactdetails left outer join users on contactdetails.smownerid=users.id left outer join user2role on users.id=user2role.userid left outer join qualification on contactdetails.qualification=qualification.qualificationid left outer join speciality on contactdetails.speciality=speciality.specialityid left outer join class on contactdetails.class=class.classid left outer join visitfrequency on contactdetails.frequency=visitfrequency.frequencyid left outer join city on city.cityid=contactdetails.mailingcity left outer join bricks on contactdetails.brick=bricks.brickid
            // left outer join brandmatrixplanning on (contactdetails.contactid = brandmatrixplanning.contactid and brandmatrixplanning.mainid='".$maxid."')
            // left outer join brandmatrixplanningmain on (brandmatrixplanningmain.mainid=brandmatrixplanning.mainid and brandmatrixplanningmain.deleted=0)
            // where contactdetails.deleted=0 AND  contactdetails.smownerid='".$current_user->id."'  ORDER BY contactdetails.firstname ASC";

            $sqlr = $adb->query($sql);
        } else if ($_REQUEST['doclist'] == 3 || $doctorlist == 3) {
            $sql = "select contactdetails.contactid as id,smcreatorid,contactdetails.firstname, contactdetails.middlename,contactdetails.lastname,qualificationname, speciality.speciality,specialityname,classname,brickname,visitfrequency.frequencyname,city.cityname
				from campaignmain
				inner join campaigndetails
				on campaignmain.crmid=campaigndetails.id
				inner join users
				on users.id=campaignmain.smcreatorid
				inner join patches
				on users.patch=patches.patchid
				inner join division
				on users.division=division.divisionid
				inner join campaigns
				on campaigns.pid=campaignmain.campaignid
				inner join contactdetails on contactdetails.contactid=campaigndetails.contactid
				left outer join qualification on contactdetails.qualification=qualification.qualificationid left outer join speciality on contactdetails.speciality=speciality.specialityid left outer join class on contactdetails.class=class.classid
				left outer join visitfrequency on contactdetails.frequency=visitfrequency.frequencyid left outer join city on city.cityid=contactdetails.mailingcity left outer join bricks on contactdetails.brick=bricks.brickid
				where campaignmain.deleted=0 and campaigndetails.del=0
				and contactdetails.smownerid='" . $current_user->id . "'
				group by smcreatorid,campaignid,contactdetails.contactid
				order by contactdetails.firstname";
            $sqlr = $adb->query($sql);
        }

        $focbrandcountq = "select noofbrands from brandmatrixmaster where division='" . $current_user->division . "' and year='" . $_REQUEST['fyear'] . "'";
        $focbrandcountr = $adb->query($focbrandcountq);
        $noofbrands     = $adb->query_result($focbrandcountr, 0, 'noofbrands');
        $focussedbrand  = '';
        for ($i = 0; $i < $noofbrands; $i++) {
            $a = $i + 1;
            $focussedbrand .= '<th>Focussed Brand ' . $a . '</th>';
        }
        $list_form->assign("FOCUSSEDBRAND", $focussedbrand);
        //to take all hidden fields including brands
        $hfields = '<div style="display: none;"><table>';
        for ($h = 0; $h < $adb->num_rows($sqlr); $h++) {
            $x = $h + 1;
            $hfields .= '<tr>';
            $hfields .= '<td><input type="hidden" name="hcontact_' . $x . '" value="' . $adb->query_result($sqlr, $h, 'id') . '" /></td><td>';
            for ($g = 0; $g < $noofbrands; $g++) {
                $hfields .= '<input type="hidden" name="hbrand_' . $x . '_' . $g . '" id="hbrandid_' . $x . '_' . $g . '" value="" />';
            }
            $hfields .= '</td></tr>';
        }
        $hfields .= '</table></div>';
        if ($_REQUEST['next'] || $_REQUEST['previous']) {
            $hfields = '<div style="display: none;"><table>';
            for ($h = 0; $h < $_REQUEST['allcount']; $h++) {
                $x = $h + 1;
                $hfields .= '<tr>';
                $hfields .= '<td><input type="hidden" name="hcontact_' . $x . '" value="' . $_REQUEST['hcontact_' . $x] . '" /></td><td>';
                for ($g = 0; $g < $noofbrands; $g++) {
                    $hfields .= '<input type="hidden" name="hbrand_' . $x . '_' . $g . '" id="hbrandid_' . $x . '_' . $g . '" value="' . $_REQUEST['hbrand_' . $x . '_' . $g] . '" />';
                }
                $hfields .= '</td></tr>';
            }
            $hfields .= '</table></div>';
            $list_form->assign("HDIV", $hfields);
            $list_form->assign("HDIVCOUNT", $_REQUEST['allcount']);
        } else {
            $list_form->assign("HDIV", $hfields);
            $list_form->assign("HDIVCOUNT", $adb->num_rows($sqlr));
        }
        $list_form->assign("CONSTANTBRANDSCOUNT", $noofbrands);
        $list_form->assign("FYEAR", $_REQUEST['fyear']);
        $list_form->assign("CYCLE", $_REQUEST['cycle']);
        $list_form->assign("EDITEDMODE", 0);
        //echo $_REQUEST['doclist'];
        if ($_REQUEST['doclist']) {
            $list_form->assign("DOCTORLISTNAME", $_REQUEST['doclist']);
        } else {
            $list_form->assign("DOCTORLISTNAME", $_REQUEST['doctorlistname']);
        }

        //to get count of records
        $recordscount = $adb->num_rows($sqlr);
        //per page records
        $pagerecords = 20;
        $last        = $recordscount / $pagerecords;
        if (!$_REQUEST['pagenum']) {
            $pagenum = 0;
        } else {
            if ($_REQUEST['next']) {
                $pagenum = $_REQUEST['pagenum'];
            } else if ($_REQUEST['previous']) {
                $pagenum = $_REQUEST['pageprevious'];
            }

        }

        if (!$_REQUEST['pagenum']) {
            $list_form->assign("PAGENUM", 2);
            $list_form->assign("PAGENUMPREV", 0);
        } else if ($_REQUEST['pagenum'] && $_REQUEST['next']) {
            $no  = $_REQUEST['pagenum'] + 1;
            $no1 = $_REQUEST['pageprevious'] + 1;
            $list_form->assign("PAGENUM", $no);
            $list_form->assign("PAGENUMPREV", $no1);
        } else if ($_REQUEST['pageprevious'] && $_REQUEST['previous']) {
            $no1 = $_REQUEST['pagenum'] - 1;
            $no  = $_REQUEST['pageprevious'] - 1;
            $list_form->assign("PAGENUM", $no1);
            $list_form->assign("PAGENUMPREV", $no);
        }
        if ($pagenum < 1) {
            $pagenum = 1;
        } else if ($pagenum > $last) {
            $pagenum = $last;
        }

        $list_form->assign("DISABLEPREVIOUS", '');
        $list_form->assign("DISABLENEXT", '');
        if ($pagenum == 1) {
            $list_form->assign("DISABLEPREVIOUS", 'disabled="disabled"');
        } else if ($pagenum == $last) {
            $list_form->assign("DISABLENEXT", 'disabled="disabled"');
        }
        $no = ceil($pagenum);
        if ($no == 0) {
            $no = 1;
        }
        $list_form->assign("PAGENUMBER", 'Page ' . $no . ' of ' . ceil($last));
        if ($doctorlist != '') {
            $max = 'limit ' . ($pagenum - 1) * $pagerecords . ',' . $pagerecords;
        } else {
            $max = 'limit ' . ($pagenum - 1) * $pagerecords . ',' . $pagerecords;
        }

        if ($_REQUEST['doclist'] == 2 || $doctorlist == 2) {
            $listname = 'M 40';
            $sql      = "select
				contactdetails.deleted as delcontact,
				bricks.brickname,
				msldetails.patchid,
				msldetails.contactownerid as userid,
				msldetails.pid,
				contactdetails.contactid as id,
				contactdetails.firstname,
				contactdetails.middlename,
				contactdetails.lastname,
				qualificationname,
				speciality.speciality,
				specialityname,
				classname,
				visitfrequency.frequencyname,
				city.cityname,
				accountname,
				case when users.rx_type='Regular' then concat_ws(' ',users.first_name,users.middle_name,users.last_name) else patches.patchname end as username ,
				msldetails.dr_type as type,
				msldetails.delrsn as delrsn
				from msllist
				left outer join msldetails on msldetails.id = msllist.crmid
				left outer join bricks on msldetails.patchid = bricks.brickid
				inner join contactdetails on contactdetails.contactid = msldetails.contactid
				inner join users on msldetails.contactownerid=users.id
				inner join patches on users.patch=patches.patchid
				left outer join class on classid=contactdetails.class
				left outer join speciality on specialityid=contactdetails.speciality
				left outer join visitfrequency on frequencyid=contactdetails.frequency
				left outer join city on city.cityid=contactdetails.mailingcity
				left outer join qualification on contactdetails.qualification=qualification.qualificationid
				where msllist.crmid='" . $crmid . "' and msllist.authorise=2 and msllist.submitted=1  and msllist.deleted=0 and msldetails.del=0  ORDER BY contactdetails.firstname ASC " . $max;
            $sqlrec = $adb->query($sql);
        } else if ($_REQUEST['doclist'] == 1 || $doctorlist == 1) {
            $listname = 'Entire list';
            $sql      = "select contactdetails.contactid as id,contactdetails.doc_code,contactdetails.contactid as crmid,contactdetails.firstname,contactdetails.middlename,contactdetails.lastname,qualificationname,speciality.speciality,specialityname,classname,brickname,users.rx_type,smownerid,concat_ws(' ',users.first_name,last_name) as user_name,visitfrequency.frequencyname,city.cityname,accountname,contactdetails.edit_available,masterid,updated from contactdetails left outer join users on contactdetails.smownerid=users.id left outer join user2role on users.id=user2role.userid left outer join qualification on contactdetails.qualification=qualification.qualificationid left outer join speciality on contactdetails.speciality=speciality.specialityid left outer join class on contactdetails.class=class.classid left outer join visitfrequency on contactdetails.frequency=visitfrequency.frequencyid left outer join city on city.cityid=contactdetails.mailingcity left outer join bricks on contactdetails.brick=bricks.brickid where contactdetails.deleted=0 AND  contactdetails.smownerid='" . $current_user->id . "'  ORDER BY contactdetails.firstname ASC " . $max;
            $sqlrec   = $adb->query($sql);
        } else if ($_REQUEST['doclist'] == 3 || $doctorlist == 3) {
            $listname = 'L 40';
            $sql      = "select contactdetails.contactid as id,smcreatorid,contactdetails.firstname, contactdetails.middlename,contactdetails.lastname,qualificationname, speciality.speciality,specialityname,classname,brickname,visitfrequency.frequencyname,city.cityname
				from campaignmain
				inner join campaigndetails
				on campaignmain.crmid=campaigndetails.id
				inner join users
				on users.id=campaignmain.smcreatorid
				inner join patches
				on users.patch=patches.patchid
				inner join division
				on users.division=division.divisionid
				inner join campaigns
				on campaigns.pid=campaignmain.campaignid
				inner join contactdetails on contactdetails.contactid=campaigndetails.contactid
				left outer join qualification on contactdetails.qualification=qualification.qualificationid left outer join speciality on contactdetails.speciality=speciality.specialityid left outer join class on contactdetails.class=class.classid
				left outer join visitfrequency on contactdetails.frequency=visitfrequency.frequencyid left outer join city on city.cityid=contactdetails.mailingcity left outer join bricks on contactdetails.brick=bricks.brickid
				where campaignmain.deleted=0 and campaigndetails.del=0
				and contactdetails.smownerid='" . $current_user->id . "'
				group by smcreatorid,campaignid,contactdetails.contactid
				order by contactdetails.firstname " . $max;
            $sqlrec = $adb->query($sql);
        }
        //to build brand dropdown
        $brandq = "select productbrandtypeid, productbrandtype from productbrandtype
			INNER JOIN brand_vs_contents ON productbrandtype.productbrandtypeid = brand_vs_contents.brand_id
			INNER JOIN content ON content.id = brand_vs_contents.content_id
			" . $innerjoin . "
			where presence=1 and  division='" . $current_user->division . "'  and brand_vs_contents.del_flag=0 and content.del_flag=0 " . $whereclause . " group by productbrandtype.productbrandtypeid";
        $brandr = $adb->query($brandq);

        //added by infipre for updating the requireent

        // $brandoption = '';
        // for ($b=0; $b<$adb->num_rows($brandr); $b++)
        // {
        //     $brandoption .= '<option value="'.$adb->query_result($brandr,$b,'productbrandtypeid').'" selected>'.$adb->query_result($brandr,$b,'productbrandtype').'</option>';
        // }

        //to get all records
        $list = '';
        $srno = ($pagenum - 1) * $pagerecords;
        $srno = $srno + 1;
        //$mquery = "select crmid from msllist where smownerid='".$current_user->id."' and fyear='".$_REQUEST['fyear']."' and deleted=0";
        $mquery   = "select max(crmid) as crmid from msllist where deleted=0 and smownerid = '" . $current_user->id . "' and msllist.authorise=2 and msllist.submitted=1 group by smownerid order by smownerid";
        $mqueryrs = $adb->query($mquery);

        // echo $adb->num_rows($sqlrec);
        //     exit;

        for ($i = 0; $i < $adb->num_rows($sqlrec); $i++) {
            $msquery   = "select contactdetails.contactid from  msllist  left outer join msldetails on msldetails.id = msllist.crmid  inner join contactdetails on contactdetails.contactid = msldetails.contactid  where msllist.crmid='" . $adb->query_result($mqueryrs, 0, 'crmid') . "'  and msllist.deleted=0 and msllist.authorise=2 and msllist.submitted=1 and msldetails.del=0 and contactdetails.contactid='" . $adb->query_result($sqlrec, $i, 'id') . "'";
            $msqueryrs = $adb->query($msquery);
            if ($adb->num_rows($msqueryrs) > 0) {
                $listname = 'M 40';
            } else {
                if ($_REQUEST['doclist'] == 1 || $doctorlist == 1) {
                    $listname = 'Entire List';
                } else if ($_REQUEST['doclist'] == 2 || $doctorlist == 2) {
                    $listname = 'M 40';
                } else if ($_REQUEST['doclist'] == 3 || $doctorlist == 3) {
                    $listname = 'L 40';
                }
            }
            $list .= '<tr>';
            $list .= '<td>' . $srno . '</td>';
            $list .= '<td>Dr ' . $adb->query_result($sqlrec, $i, 'firstname') . ' ' . $adb->query_result($sqlrec, $i, 'middlename') . ' ' . $adb->query_result($sqlrec, $i, 'lastname') . '</td>';
            $list .= '<td>' . $adb->query_result($sqlrec, $i, 'speciality') . '</td>';
            $list .= '<td>' . $adb->query_result($sqlrec, $i, 'frequencyname') . '</td>';
            $list .= '<td>' . $listname . '</td>';
            if ($_REQUEST['next'] || $_REQUEST['previous']) {
                for ($h = 0; $h < $_REQUEST['allcount']; $h++) {
                    $x = $h + 1;
                    if ($x == $srno) {
                        for ($g = 0; $g < $noofbrands; $g++) {
                            $brandq = "select productbrandtypeid, productbrandtype from productbrandtype
								INNER JOIN brand_vs_contents ON productbrandtype.productbrandtypeid = brand_vs_contents.brand_id
								INNER JOIN content ON content.id = brand_vs_contents.content_id
								" . $innerjoin . "
								where presence=1 and  division='" . $current_user->division . "'  and brand_vs_contents.del_flag=0 and content.del_flag=0 " . $whereclause . " group by productbrandtype.productbrandtypeid";
                            $brandr      = $adb->query($brandq);
                            $brandoption = '';
                            for ($b = 0; $b < $adb->num_rows($brandr); $b++) {
                                if ($_REQUEST['hbrand_' . $x . '_' . $g] == $adb->query_result($brandr, $b, 'productbrandtypeid')) {
                                    $select = 'selected="selected"';
                                } else {
                                    $select = '';
                                }
                                $brandoption .= '<option value="' . $adb->query_result($brandr, $b, 'productbrandtypeid') . '" ' . $select . '>' . $adb->query_result($brandr, $b, 'productbrandtype') . '</option>';
                            }
                            $list .= '<td>
								<select name="brand_' . $srno . '" id="brand_' . $srno . '_' . $g . '" onClick="updateField(this.id)" onBlur="updateField(this.id)">
								<option style="background:yellow" value="">Select Brand</option>
								' . $brandoption . '
								</select>
								</td>';
                        }
                        $list .= '</tr>';
                    }
                }
            } else {
                $get_brand = explode(',', get_brands($maxid, $adb->query_result($sqlrec, $i, 'id')));
                for ($j = 0; $j < $noofbrands; $j++) {

                    $list .= '<td>
									<select name="brand_' . $srno . '" id="brand_' . $srno . '_' . $j . '" onChange="updateField(this.id);" onBlur="updateField(this.id)">
									<option style="background:yellow" value="">Select Brand</option>';
                    for ($b = 0; $b < $adb->num_rows($brandr); $b++) {
                        if (isset($get_brand[$j])) {
                            if ($get_brand[$j] == $adb->query_result($brandr, $b, 'productbrandtypeid')) {
                                $selected_brand = 'selected';
                            } else {
                                $selected_brand = '';
                            }

                        }
                        $list .= '<option value="' . $adb->query_result($brandr, $b, 'productbrandtypeid') . '" ' . $selected_brand . '>' . $adb->query_result($brandr, $b, 'productbrandtype') . '</option>';
                        $selected_brand = '';
                    }

                    $list .= '	</select>
								</td>';
                }
                $list .= '</tr>';
            }
            $srno++;
        }

        $sql = "select contactdetails.*,brandmatrixplanning.brandsfocussed, speciality.speciality,  visitfrequency.frequencyname from contactdetails left outer join users on contactdetails.smownerid=users.id left outer join user2role on users.id=user2role.userid left outer join qualification on contactdetails.qualification=qualification.qualificationid left outer join speciality on contactdetails.speciality=speciality.specialityid left outer join class on contactdetails.class=class.classid left outer join visitfrequency on contactdetails.frequency=visitfrequency.frequencyid left outer join city on city.cityid=contactdetails.mailingcity left outer join bricks on contactdetails.brick=bricks.brickid
				left outer join brandmatrixplanning on (contactdetails.contactid = brandmatrixplanning.contactid and brandmatrixplanning.mainid='" . $maxid . "')
				left outer join brandmatrixplanningmain on (brandmatrixplanningmain.mainid=brandmatrixplanning.mainid and brandmatrixplanningmain.deleted=0)
				where contactdetails.deleted=0 AND  contactdetails.smownerid='" . $current_user->id . "'  ORDER BY contactdetails.firstname ASC";
        //print_r($sql);exit;
        $sqlr = $adb->query($sql);
        ///added by infipre////
        $sqlnumrows    = $adb->num_rows($sqlr);
        $prevcontactid = array();
        for ($vw = 0; $vw < $sqlnumrows; $vw++) {
            $prevcontactid[$adb->query_result($sqlr, $vw, 'contactid')] = $adb->query_result($sqlr, $vw, 'brandsfocussed');
        }
        //end//
        $brandmatrixq   = "select year from brandmatrixplanningmain where mainid='" . $maxid . "'";
        $brandmatrixr   = $adb->query($brandmatrixq);
        $focbrandcountq = "select noofbrands from brandmatrixmaster where division='" . $current_user->division . "' and year='" . $adb->query_result($brandmatrixr, 0, 'year') . "'";
        // $focbrandcountq = "select noofbrands from brandmatrixmaster where division='" . $current_user->division . "' and year='2019'";
        $focbrandcountr = $adb->query($focbrandcountq);
        $noofbrands     = $adb->query_result($focbrandcountr, 0, 'noofbrands');
        $focussedbrand  = '';
        for ($i = 0; $i < $noofbrands; $i++) {
            $a = $i + 1;
            $focussedbrand .= '<th>Focussed Brand ' . $a . '</th>';
        }
        $list_form->assign("FOCUSSEDBRAND", $focussedbrand);
        //to take all hidden fields including brands
        $hfields = '<div style="display: none;"><table id="detaildata">';
        //////////////added by Infipre///////////////
        $cnt_brands = $adb->num_rows($sqlr);
        if ($cnt_brands > 0) {
            for ($h = 0; $h < $adb->num_rows($sqlrec); $h++) {
                $x = $h + 1;
                $hfields .= '<tr>';
                $hfields .= '<td><input type="hidden" id="hcontact_' . $x . '" name="hcontact_' . $x . '" value="' . $adb->query_result($sqlrec, $h, 'id') . '" /></td><td>';
                $docid = $adb->query_result($sqlrec, $h, 'id');
                if (array_key_exists($docid, $prevcontactid)) {
                    $brands = explode(",", $prevcontactid[$docid]);

                    for ($g = 0; $g < $noofbrands; $g++) {
                        if (count($brands) > $g) {
                            if ($brands[$g]) {
                                $val = $brands[$g];
                            } else {
                                $val = '';
                            }

                        } else {
                            $val = '';
                        }
                        $hfields .= '<input type="hidden" name="hbrand_' . $x . '_' . $g . '" id="hbrandid_' . $x . '_' . $g . '" value="' . $val . '" />';
                    }
                } else {
                    for ($g = 0; $g < $noofbrands; $g++) {
                        $val = '';
                        $hfields .= '<input type="hidden" name="hbrand_' . $x . '_' . $g . '" id="hbrandid_' . $x . '_' . $g . '" value="' . $val . '" />';
                    }

                }
                $hfields .= '</td></tr>';
            }
        } else {
            for ($bm = 0; $bm < $adb->num_rows($sqlrec); $bm++) {
                $bm1 = $bm + 1;
                $hfields .= '<tr>';
                $hfields .= '<td><input type="hidden" id="hcontact_' . $bm1 . '" name="hcontact_' . $bm1 . '" value="' . $adb->query_result($sqlrec, $bm, 'id') . '" /></td><td>';
                for ($g = 0; $g < $noofbrands; $g++) {

                    $val = '';
                    $hfields .= '<input type="hidden" name="hbrand_' . $bm1 . '_' . $g . '" id="hbrandid_' . $bm1 . '_' . $g . '" value="' . $val . '" />';
                }
                $hfields .= '</td></tr>';
            }
        }
        ///////////////////////End/////////////////////////////////////////////////////
        $hfields .= '</table></div>';
        $list_form->assign("HDIV", $hfields);
        $list_form->assign("LISTOFRECORDS", $list);
        $list_form->assign("MODE", 'new');
        $list_form->assign("RECORD", $_REQUEST['record']); //dg
        $list_form->parse("main");
        $list_form->out("main");
    }
}

function get_brands($id, $contactid)
{
    $q = "select brandsfocussed from brandmatrixplanning where mainid=$id and contactid=$contactid and deleted=0";
    $r = mysql_query($q);
    $o = mysql_fetch_assoc($r);
    return $o['brandsfocussed'];
}

?>


