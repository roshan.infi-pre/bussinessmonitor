<?php

$formdata   = $_POST['formdata'];
$count      = $_POST['count'];
$noofbrands = $_POST['noofbrands'];

foreach ($formdata as $key => $v) {
    for ($i = 1; $i <= $count; $i++) {
        if ($v['name'] == 'hcontact_' . $i) {
            $doc_details[$i] = get_doc_details($v['value']);

        }
        for ($j = 0; $j < $noofbrands; $j++) {
            if ($v['name'] == 'hbrand_' . $i . '_' . $j) {
                //$brandname = get_brand_name($v['value']);
                $doc_details[$i]['brand_' . $j] = get_brand_name($v['value']);
            }

        }

    }
}

// echo '<pre>';
// print_r($doc_details);
// exit;

$get_str = get_str($doc_details, $noofbrands);

echo $get_str;
exit;

function get_doc_details($id)
{
    $sql = "select contactdetails.contactid as id,contactdetails.firstname, contactdetails.middlename,contactdetails.lastname,qualification.qualificationname, speciality.speciality,specialityname,classname,brickname,visitfrequency.frequencyname,city.cityname
    from contactdetails
    inner join speciality on contactdetails.speciality=speciality.specialityid
    inner join visitfrequency on contactdetails.frequency=visitfrequency.frequencyid
    inner join city on city.cityid=contactdetails.mailingcity
    inner join qualification on contactdetails.qualification=qualification.qualificationid
    inner join bricks on contactdetails.brick=bricks.brickid
    inner join class on contactdetails.class=class.classid
    where contactdetails.deleted =0 and contactdetails.contactid=" . $id . "";
    //print_r($sql);exit;
    $r = mysql_query($sql);
    $o = mysql_fetch_assoc($r);
    return $o;
}
function get_brand_name($brandid)
{
    $sql = "select productbrandtypeid, productbrandtype from productbrandtype where productbrandtypeid=" . $brandid . "";
    $r   = mysql_query($sql);
    $o   = mysql_fetch_assoc($r);
    return $o['productbrandtype'];
}
function get_str($data, $noofbrands)
{
    $srn = 1;
    $str = '';
    $str .= '<table border="0" class="table details table-fixed"><thead>';
    $str .= '<tr>';
    $str .= '<th width="50">Sr.No</th><th width="200">Doctor Name</th><th width="200">Speciality</th><th width="50">Frequency</th><th width="100">BM</th>';
    for ($j = 1; $j <= $noofbrands; $j++) {
        $str .= '<th width="150">Focussed Brand ' . $j . '</th>';
    }

    $str .= '</tr></thead></table>';
    $str .= '<div style="overflow-y:scroll;max-height:500px;"><table border="0" class="table details table-fixed"><tbody>';
    foreach ($data as $key => $value) {
        $str .= '<tr>';
        $str .= '<td width="50" style="border-bottom:1px solid #000;">' . $srn . '</td>';
        $str .= '<td width="200" style="border-bottom:1px solid #000;">Dr.' . $value['firstname'] . ' ' . $value['middlename'] . ' ' . $value['lastname'] . '</td>';
        $str .= '<td width="200" style="border-bottom:1px solid #000;">' . $value['specialityname'] . '</td>';
        $str .= '<td width="50" style="border-bottom:1px solid #000;">' . $value['frequencyname'] . '</td>';
        $str .= '<td width="100" style="border-bottom:1px solid #000;">Entire List</td>';
        for ($j = 0; $j < $noofbrands; $j++) {
            $str .= '<td width="150" style="border-bottom:1px solid #000;">' . $value['brand_' . $j] . '</td>';
        }

        $str .= '</tr>';
        $srn++;
    }
    $str .= '</tbody></table></div>';
    return $str;

}
