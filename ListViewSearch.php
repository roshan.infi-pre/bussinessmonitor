<?php
require_once 'XTemplate/xtpl.php';
require_once 'themes/' . $theme . '/layout_utils.php';

require_once 'include/utils.php';
require_once 'include/uifromdbutil.php';
require_once 'include/database/PearDatabase.php';

global $app_strings;
global $app_list_strings;
global $mod_strings;
//print_r($_REQUEST);
// echo $current_language;
$current_module_strings = return_module_language($current_language, $_REQUEST["module"]);
//print_r($app_strings);

$profileID = fetchUserProfileId($current_user->id);
$fyear     = $_REQUEST['fyear'];
if ($fyear != "") {
    $fyearvalue                   = explode('_', $fyear);
    $_SESSION['latestfyeartable'] = $fyear;
    $_SESSION['latestfyearwhere'] = $fyearvalue[0] . "-" . $fyearvalue[1];
} else {
    $yearcondition1               = date('Y-m-d');
    $finacialyear_query1          = "select financial_year from financialyear where '" . $yearcondition1 . "' between from_date and to_date";
    $financial_res1               = $adb->query($finacialyear_query1);
    $fyear1                       = $adb->query_result($financial_res1, 0, "financial_year");
    $_SESSION['latestfyeartable'] = '';
    $_SESSION['latestfyearwhere'] = $fyear1;
}

$q = "update brandmatrixplanningmain set deleted=1 where mainid = 186"; //182, 184, 185
$adb->query($q);
// $adb->query("update brandmatrixplanningmain set deleted=1 where mainid = 182");
// $adb->query("update brandmatrixplanningmain set deleted=1 where mainid = 184");
// $adb->query("update brandmatrixplanningmain set deleted=1 where mainid = 185");
// echo $q;
// exit;

// modules where alphabetical search is applicable
$aAppModules     = array('Doctors' => 'contactdetails.' . $_SESSION['latestfyeartable'] . 'lastname', 'Chemists' => 'chemistname', 'Stockists' => 'stockistname');
$aSystemVarReplc = array('[Y]' => $_SESSION['latestfyeartable']);
$sModuleName     = $_REQUEST['module'];
$sActionName     = $_REQUEST['action'];
// [Ticket_ID:0382307]
$smenuid = $_REQUEST["menuid"];

//echo $sModuleName . $sActionName;
$aModuleListParams['Doctors']               = array('listpage' => 'EditView', 'moduleobject' => 'Contact', 'modulename' => 'Contacts', 'modulequery' => 'LatestContacts', 'order_by' => '', 'sorder' => '');
$aModuleListParams['Monthly Tour Plan']     = array('listpage' => 'EditView', 'moduleobject' => 'TourPlan', 'modulename' => 'TourPlans', 'modulequery' => 'TourPlans', 'order_by' => 'touringyearmonth.year,touringyearmonth.month', 'sorder' => '');
$aModuleListParams['Brand Matrix Planning'] = array('listpage' => 'brandMatrixEditView', 'moduleobject' => 'brandMatrix', 'modulename' => 'TourPlans', 'modulequery' => 'BrandMatrixPlanning', 'order_by' => 'brandmatrixplanningmain.financialyear,brandmatrixplanningmain.cycle', 'sorder' => '');

$sSQL = "SELECT tm.menuid, tm.label, tm.no_listing_ids, ms.menusearchid, search_filter, search_type, search_case, sp.search_prefill,
		search_change_ids, ms.menu_orderby, ms.db_field_name, ms.php_file_variable, t.name, tm.filename, tm.filepath, sp.is_mandatory
		   FROM tab t, tab2menu as tm
			LEFT OUTER JOIN menu2search as ms ON (tm.menuid=ms.menuid AND ms.deleted=0)
			INNER JOIN search2profile as sp ON (ms.menusearchid=sp.menusearchid AND sp.profileid=" . $profileID . ")
		  WHERE tm.deleted=0
			AND tm.menuid=" . $_REQUEST["menuid"] . "
			AND tm.tabid=t.tabid
		 ORDER BY ms.menu_orderby";
//echo $sSQL;
$res     = $adb->query($sSQL);
$numrows = $adb->num_rows($res);
for ($i = 0; $i < $numrows; $i++) {
    $iMenuId       = $adb->query_result($res, $i, "menuid");
    $iLable        = $adb->query_result($res, $i, "label");
    $iNolistIds    = $adb->query_result($res, $i, "no_listing_ids");
    $iMSrchId      = $adb->query_result($res, $i, "menusearchid");
    $iSrchFilNm    = $adb->query_result($res, $i, "search_filter");
    $iSrchFilTy    = $adb->query_result($res, $i, "search_type");
    $iSrchFilCs    = $adb->query_result($res, $i, "search_case");
    $iSrchPreFil   = $adb->query_result($res, $i, "search_prefill");
    $iSrchChngIds  = $adb->query_result($res, $i, "search_change_ids");
    $iSrchDBflds   = $adb->query_result($res, $i, "db_field_name");
    $iSrchPhpVarNm = $adb->query_result($res, $i, "php_file_variable");
    $iTabNm        = $adb->query_result($res, $i, "name");
    $iFileNm       = $adb->query_result($res, $i, "filename");
    $iFilePath     = $adb->query_result($res, $i, "filepath");
    $isMandatory   = $adb->query_result($res, $i, "is_mandatory");

    $aSearchFilter[$iMSrchId]["Name"]      = $iSrchFilNm;
    $aSearchFilter[$iMSrchId]["id"]        = $iMSrchId;
    $aSearchFilter[$iMSrchId]["case"]      = $iSrchFilCs;
    $aSearchFilter[$iMSrchId]["type"]      = $iSrchFilTy;
    $aSearchFilter[$iMSrchId]["prefill"]   = $iSrchPreFil;
    $aSearchFilter[$iMSrchId]["changeIds"] = explode(',', $iSrchChngIds);
    $aSearchFilter[$iMSrchId]["dbfields"]  = $iSrchDBflds;
    $aSearchFilter[$iMSrchId]["PhpVarNm"]  = $iSrchPhpVarNm;
    $aSearchFilter[$iMSrchId]["Mandatory"] = $isMandatory;
}
//check whether default listing is applicable
$isListing = 1;
if ($iNolistIds) {
    $aNolistIds = explode(',', $iNolistIds);
    if (in_array($profileID, $aNolistIds)) {
        unset($isListing);
    }
}
// for radio auto search
$sSQL = "SELECT searchmodid, menuid, action_case, search_fields, search_module, s.header_id, headers, search_label, db_field_name, php_file_variable
		   FROM search2module s, search2header h
		  WHERE search_module='" . $_REQUEST["module"] . "'
			AND action_case='" . $_REQUEST["action"] . "'
			AND menuid='" . $iMenuId . "'
			AND s.header_id=h.header_id
		 ORDER BY search_orderby";
//echo $sSQL;
$res     = $adb->query($sSQL);
$numrows = $adb->num_rows($res);
for ($i = 0; $i < $numrows; $i++) {
    $iSearchmodId   = $adb->query_result($res, $i, "searchmodid");
    $imenuId        = $adb->query_result($res, $i, "menuid");
    $iaction_case   = $adb->query_result($res, $i, "action_case");
    $isearch_fields = $adb->query_result($res, $i, "search_fields");
    $isearch_module = $adb->query_result($res, $i, "search_module");
    $iheader_id     = $adb->query_result($res, $i, "header_id");
    $sheaders       = $adb->query_result($res, $i, "headers");
    $search_label   = $adb->query_result($res, $i, "search_label");
    $search_db_fld  = $adb->query_result($res, $i, "db_field_name");
    $search_php_var = $adb->query_result($res, $i, "php_file_variable");

    $aSearchRadio[$isearch_fields]["Name"]         = $isearch_fields;
    $aSearchRadio[$isearch_fields]["id"]           = $iSearchmodId;
    $aSearchRadio[$isearch_fields]["case"]         = $iaction_case;
    $aSearchRadio[$isearch_fields]["headerDets"]   = $iheader_id . "|" . $sheaders;
    $aSearchRadio[$isearch_fields]["search_label"] = $search_label;
    $aSearchRadio[$isearch_fields]["dbfields"]     = $search_db_fld;
    $aSearchRadio[$isearch_fields]["PhpVarNm"]     = $search_php_var;

}
// print_r($app_strings);
/*
echo "<pre>";
print_r($aSearchFilter);
print_r($aSearchRadio);
echo "</pre>";
 */

echo "\n<BR>\n";
echo "<fieldset><legend><font color='red' ><b>" . $iLable . " Search:</b></font></legend>";
echo "\n<BR>\n";
if (!isset($_REQUEST['search_form']) || $_REQUEST['search_form'] != 'false') {
    $search_form = new XTemplate('modules/TourPlans/ListViewSearchForm.html');
    $search_form->assign("MOD", $current_module_strings);
    $search_form->assign("APP", $app_strings);
    $htmlText        = '';
    $iTableColumnCnt = 4;
    $iCnt            = 0;
    $load_funcs      = '';
    $sMandJavaScript = " <script type='text/javascript' language='JavaScript'> arrMandFlds = new Array(); ";

    foreach ($aSearchFilter as $Srhkey => $aFieldsDets) {
        //echo     $aFieldsDets["Name"];
        if ($iCnt != 0 && ($iCnt % $iTableColumnCnt == 0)) {
            $htmlText .= "</tr>";
        }

        if ($iCnt == 0 || ($iCnt % $iTableColumnCnt == 0)) {
            $htmlText .= "<tr>";
        }

        if ($aFieldsDets['Mandatory'] == 1) {
            $smand   = "<font color='red'>*</font>";
            $sManDet = $FldId . "||" . $aFieldsDets["Name"];
            $sMandJavaScript .= " arrMandFlds.push('$sManDet'); ";
        } else {
            $smand = "";
        }

        $htmlText .= "<td class='dataLabel' noWrap>" . $smand . "" . $aFieldsDets["Name"] . ":</td>";
        $FldNm = "name = '" . $aFieldsDets["Name"] . "'";

        if ($aFieldsDets["type"] == 'option') {
            $PrefilCombotxt = $sReferedFunc = $Casestr = $sMultiSelect = '';
            if ($aFieldsDets["changeIds"][0] != 0) {
                foreach ($aFieldsDets["changeIds"] as $ikey => $iNewMenuId) {
                    $sLoadCase = $aSearchFilter[$iNewMenuId]["case"] . "__" . $iNewMenuId;
                    if ($sLoadCase) {
                        $Casestr .= $sLoadCase . ",";
                    }

                }
                $Casestr      = substr($Casestr, 0, -1);
                $sReferedFunc = (trim($Casestr) == '') ? '' : "onchange = funcComboLoad('" . $aFieldsDets["case"] . "__" . $aFieldsDets["id"] . "|" . $Casestr . "')";
            }
            $FldId = $aFieldsDets["case"] . "__" . $aFieldsDets["id"];
            if ($aFieldsDets["prefill"] == 1) {
                //if the combo is prefill on load
                $PrefilCombotxt = "<option value=''>--SELECT--</option>";
                // [Ticket_ID:0382307]  Product Priority setup in Myday-Urgent
                $selected = $_REQUEST[$aFieldsDets["case"]];
                if ($smenuid == 4) {
                    if ($aFieldsDets["case"] == "Year") {
                        $selected = date('Y');
                    }
                }
                //    $PrefilCombotxt .= get_select_options_with_id(get_combo($aFieldsDets["case"], ""),$_REQUEST[$aFieldsDets["case"]]);

                $PrefilCombotxt .= get_select_options_with_id(get_combo($aFieldsDets["case"], ""), $selected);
                $FldNm = "name = '" . $FldId . "'";
            }
            if ($aFieldsDets["prefill"] == 2) {
                //if the combo is multiselect and not prefilled on load
                $sMultiSelect = " multiple='true' ";
                $FldNm        = "name = '" . $aFieldsDets["Name"] . "[]' size='1' ";
            }
            if ($aFieldsDets["prefill"] == 3) {
                //if the combo is multiselect and prefilled on load
                $sMultiSelect   = " multiple='true' ";
                $FldNm          = "name = '" . $aFieldsDets["Name"] . "[]' size='1' ";
                $PrefilCombotxt = "<option>--SELECT--</option>";
                //  [Ticket_ID:0382307]: Product Priority setup in Myday-Urgent
                if ($smenuid == 4) {
// ,'',"true"
                    $PrefilCombotxt .= get_select_options_with_id(get_combo($aFieldsDets["case"], ""), $_REQUEST[$aFieldsDets["case"]]);

                } else {
                    $PrefilCombotxt .= get_select_options_with_id(get_combo($aFieldsDets["case"], ""), $_REQUEST[$aFieldsDets["case"]]);
                }
                $load_funcs .= "<script type='text/javascript' language='JavaScript'>funcComboLoad('|" . $aFieldsDets["case"] . "__" . $aFieldsDets["id"] . "');</script>";
            }
            $PrefilCombotxt = "<SELECT id='" . $FldId . "'  " . $sReferedFunc . " " . $sMultiSelect . " " . $FldNm . ">" . $PrefilCombotxt . "</SELECT>";
            $htmlText .= "<td id='td" . $aFieldsDets["case"] . "__" . $aFieldsDets["id"] . "'>" . $PrefilCombotxt . "</td>";

        }
        if ($aFieldsDets["type"] == 'text') {
            $htmlText .= "<td><input size='10' type='text' id='" . $aFieldsDets["case"] . "__" . $aFieldsDets["id"] . "' " . $FldNm . "></td>";
            $FldId = $aFieldsDets["case"] . "__" . $aFieldsDets["id"];
        }
        $iCnt++;
        //$aReqNames[$aFieldsDets["Name"]] = array($aFieldsDets["dbfields"],  $aFieldsDets["PhpVarNm"], $FldId);
        $aReqNames[$FldId] = array($aFieldsDets["dbfields"], $aFieldsDets["PhpVarNm"], $aFieldsDets["Name"]);

    }
    $sMandJavaScript .= " </script>";

    //for radio
    $htmlText .= "</tr>";
    if (is_array($aSearchRadio)) {
        $htmlText .= "<tr><td class='datalabel'>Search By</td><td class='subtext' noWrap valign='middle' colspan=7><label>";
        $i   = 0;
        $sel = '';
        foreach ($aSearchRadio as $Srhkey => $aFieldsDets) {
            if ($i == 0) {
                $sel = 'checked';
            } else {
                $sel = '';
            }
            $FldId = 'radsrchId#' . $aFieldsDets["id"];
            $htmlText .= "<input type='radio' name='radSearchBy' id='" . $FldId . "' " . $sel . " value='" . $aFieldsDets["Name"] . "|" . $aFieldsDets["headerDets"] . "|" . $FldId . "' /></label>" . $aFieldsDets["search_label"] . "";
            $i++;

            $aReqNames[$FldId] = array($aFieldsDets["dbfields"], $aFieldsDets["PhpVarNm"], $FldId, 'radSearchBy');
        }
        $htmlText .= "</td></tr><tr><td class='datalabel'>&nbsp;</td>
					<td colspan='5'><div><input type='text' size='50' value='' name='nmadvsrchId' id='inputString' onkeyup='lookup(this.value);' onblur='fill();' />
					</div><INPUT type='hidden' name='queryId' id='queryId' value='' >
					</td>";
    }

    $htmlText .= "<td align='right' colspan='8'>
			<input title='" . $app_strings['LBL_SEARCH_BUTTON_TITLE'] . "' accessKey='" . $app_strings['LBL_SEARCH_BUTTON_KEY'] . "' class='btn' type='submit' name='button' id='button' value='" . $app_strings['LBL_SEARCH_BUTTON_LABEL'] . "' onclick= \"return validateDataInputs();\" />
			<input title='" . $app_strings['LBL_CLEAR_BUTTON_TITLE'] . "' accessKey='" . $app_strings['LBL_CLEAR_BUTTON_KEY'] . "' onclick='clear_form(this.form);' class='btn' type='button' name='clear' value='" . $app_strings['LBL_CLEAR_BUTTON_LABEL'] . "' /></td>
		     </td>
		     </tr>";
    if (is_array($aSearchRadio)) {
        $htmlText .= "<tr><td colspan='1'></td>
			<td><div class='suggestionsBox' id='suggestions' style='display:none'>
				<div class='suggestionList' id='autoSuggestionsList'>&nbsp;</div>
			</div>
			<div id='divloadImg' style='display:none'><img src='themes/images/loading4.gif'></div>
			</td></tr>";
    }
    if (array_key_exists($iLable, $aAppModules)) {
        $htmlText .= "<input type='hidden' id='txtAlpha' name='txtAlpha' value=''>";
    }

    $search_form->assign("APPEND_SEARCH_HTML", $htmlText);
    if (array_key_exists($iLable, $aAppModules)) {
        $search_form->assign('ALPHABETICAL', AlphabeticalSearchNew());
    }

    $search_form->assign("JAVASCRIPT", get_clear_form_js());
    //echo $iFilePath.$iFileNm.'_subsectionlistViewjs.php';

    if (file_exists($iFilePath . $iFileNm . '_subsectionlistViewjs.php')) {
        require_once $iFilePath . $iFileNm . '_subsectionlistViewjs.php';
        $search_form->assign("JAVASCRIPT_SUB_SECTION", callsubsectionjs());
        if ($iFilePath) {
            list($filePathMainMod, $filePathSubMod) = explode('/', $iFilePath);
        }
    }

    /*
    require_once($iFileNm.'_subsectionlistViewjs.php');
    $search_form->assign("JAVASCRIPT_SUB_SECTION", callsubsectionjs());
     */
    $search_form->assign("MENUID", $menuid);
    $search_form->parse("main");
    $search_form->out("main");
}

echo "</fieldset>";
echo "\n<BR>\n";
echo "<div id='divAddHtml'></div>";
echo "\n<BR>\n";
echo "<fieldset id='fldListing' style='display:none'><legend><font color='red' ><b>" . $iLable . " Listing:</b></font></legend>";
echo "<div id='listviewdiv' width='100%' align='center'  class='container' style='display:none'><img src='themes/images/loading4.gif'></div>";
echo "</fieldset>";
echo "\n<BR>\n";

$fileModule     = $_REQUEST["module"];
$fileAjaxModule = $iTabNm;
if ($filePathSubMod) {
    $fileModule     = $filePathSubMod;
    $fileAjaxModule = $filePathSubMod;
}

echo "<input type='hidden' value='" . $fileModule . "' name='lstmodule' id='lstmodule'>
	<input type='hidden' value='" . $iFileNm . '_subsectionlistView' . "' name='lstaction' id='lstaction'>
	<input type='hidden' value='" . $iFileNm . "' name='lstcase' id='lstcase'>";

/*
echo  "<input type='hidden' value='".$_REQUEST["module"]."' name='lstmodule' id='lstmodule'>
<input type='hidden' value='".$iFileNm.'_subsectionlistView'."' name='lstaction' id='lstaction'>
<input type='hidden' value='".$iFileNm."' name='lstcase' id='lstcase'>";
 */
//echo "<pre>";
//print_r($_REQUEST);
//print_r($aReqNames);
//echo "</pre>";

if (isset($_REQUEST['button']) || isset($_REQUEST['txtAlpha']) || isset($isListing)) {
    //echo "xxxxxxx".$_REQUEST['SelectBy'][0]."aaaaaaaap";
    $url_string    = ''; // assigning http url string
    $sorder        = 'ASC'; // Default sort order
    $where_clauses = array(); // where array
    $url_string .= '&query=true&lstmodule=' . $_REQUEST['module'] . '&lstaction=' . $iTabNm . '_subsectionlistView&lstcase=' . $iTabNm;
    if ($_REQUEST['txtAlpha']) {
        array_push($where_clauses, $aAppModules[$iLable] . " LIKE '" . $_REQUEST['txtAlpha'] . "%'");
    }
    // echo "<pre>";
    // print_r($aReqNames);
    // print_r($_REQUEST);
    // echo "</pre>";

    foreach ($aReqNames as $Id => $aVals) {
        //print_r($aVals);
        foreach ($aSystemVarReplc as $rplc => $rplmnt) {
            $aVals[0] = str_replace($rplc, $rplmnt, $aVals[0]);
        }

        if (is_array($_REQUEST[$Id])) {
            // for multiselect
            $reqValue = implode(',', $_REQUEST[$Id]);
            if (trim($aVals[0])) {
                array_push($where_clauses, $aVals[0] . " IN (" . $reqValue . ")");
            }

            if (trim($aVals[1])) {
                $url_string .= "&" . $aVals[1] . "=" . $reqValue;
            }

        } else {
            if ($_REQUEST[$Id] == '') {
                if ($_REQUEST[$aReqNames[$Id][2]]) {
                    if (trim($aVals[0])) {
                        array_push($where_clauses, $aVals[0] . "='" . $_REQUEST[$aReqNames[$Id][2]] . "'");
                    }

                    if (trim($aVals[1])) {
                        $url_string .= "&" . $aVals[1] . "=" . $_REQUEST[$aReqNames[$Id][2]];
                    }

                } else {
                    $aRadSearchBy = explode('|', $_REQUEST['radSearchBy']);
                    if (($aRadSearchBy[3] == $Id) && $_REQUEST['queryId']) {
                        if (trim($aVals[0])) {
                            array_push($where_clauses, $aVals[0] . "='" . $_REQUEST['queryId'] . "'");
                        }

                        if (trim($aVals[1])) {
                            $url_string .= "&" . $aVals[1] . "=" . $_REQUEST['queryId'];
                        }

                    }
                }
            } else {
                if (trim($aVals[0])) {
                    array_push($where_clauses, $aVals[0] . "='" . $_REQUEST[$Id] . "'");
                }

                if (trim($aVals[1])) {
                    $url_string .= "&" . $aVals[1] . "=" . $_REQUEST[$Id];
                }

            }
        }
    }
    // addition parameters per menu wise
    foreach ($aModuleListParams[$iLable] as $ikey => $sVal) {
        $url_string .= '&' . $ikey . '=' . $sVal;
    }
    $where = "";
    foreach ($where_clauses as $clause) {
        if ($where != "") {
            $where .= " and ";
        }

        $where .= $clause;
    }
    //echo $where."<br><br>";
    //echo $url_string."<br>";
    //exit;
    ?>
<script language='javascript'>
	var url="<?echo $url_string; ?>";
	var where="<?echo $where; ?>";
	var module ="<?echo $iFileNm; ?>";
	//alert(url +'aaaaaaaaa'+ where +'aaaaaaaaa'+ module) ;
	//alert(where);
	AjaxfunListViewShow_New(url, where, module);
</script>

<?php
}
echo $load_funcs;
echo $sMandJavaScript;
function AlphabeticalSearchNew()
{
    $list .= "<td>Search by Last Name:</td>";
    for ($var = 'A', $i = 1; $i <= 26; $i++, $var++) {
        $list .= "<td class='alphaBg'><a href=\"javascript:clickAlphabet('" . $var . "');\" onclick=\"return validateDataInputs();\">" . $var . "</a></td>";
    }

    return $list;
}
?>